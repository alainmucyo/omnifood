$(function () {
    $(".js--section-features").waypoint(function (direction) {
            if (direction == "down") {
                $("nav").addClass("sticky")
            } else {
                $("nav").removeClass("sticky")
            }
        },
        {offset: '60px'})
});
$(function () {
    $('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});
$(".js--wp-1").waypoint(function (direction) {
        $(".js--wp-1").addClass("animated fadeIn")
    },
    {
        offset: '50%'
    }
);
$(".js--wp-2").waypoint(function (direction) {
        $(".js--wp-2").addClass("animated fadeInUp")
    },
    {
        offset: '50%'
    }
);
$(".js--wp-3").waypoint(function (direction) {
        $(".js--wp-3").addClass("animated pulse")
    },
    {
        offset: '50%'
    }
);
$(".js--wp-4").waypoint(function (direction) {
        $(".js--wp-4").addClass("animated fadeIn")
    },
    {
        offset: '50%'
    }
);

$(".mobile-nav-icon").click(function () {
    $(".js--main-nav").slideToggle(200);
    var nav=$("#nav-icon");
    if(nav.hasClass("ion-navicon")) {
        nav.addClass("ion-android-close")
        nav.removeClass("ion-navicon")
    }
    else{
        nav.addClass("ion-navicon")
        nav.removeClass("ion-android-close")
    }
});